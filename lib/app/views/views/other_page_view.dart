import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:getx_example/app/modules/home/controllers/home_controller.dart';

class OtherPageView extends GetView {
  HomeController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('OtherPageView'),
        centerTitle: true,
      ),
      body: Obx(
        () => Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: Text(
                'Counter adalah : ${controller.count}',
                style: TextStyle(fontSize: 20),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            ElevatedButton(
              onPressed: () => Get.back(),
              child: const Text('Back To Home'),
            ),
          ],
        ),
      ),
    );
  }
}
