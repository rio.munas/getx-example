import 'package:get/get.dart';
import 'package:getx_example/app/controllers/auth_controller.dart';

class HomeController extends GetxController {
  //TODO: Implement HomeController

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void logout() async {
    AuthController authc = Get.find();
    authc.logout();
  }

  void increment() => count.value++;
  void decrement() => count.value--;
}
