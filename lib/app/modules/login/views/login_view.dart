import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:getx_example/app/modules/login/controllers/login_controller.dart';
import 'package:getx_example/app/routes/app_pages.dart';

class LoginView extends GetView<LoginController> {
  final emailC = TextEditingController(text: 'getx@flutter.com');
  final passC = TextEditingController(text: '123456');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
        centerTitle: true,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
            child: TextField(
              controller: emailC,
              decoration: InputDecoration(labelText: 'E-mail'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
            child: TextField(
              controller: passC,
              decoration: InputDecoration(labelText: 'Password'),
              obscureText: true,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          ElevatedButton(
            onPressed: () {
              controller.login(emailC.text, passC.text);
            },
            child: Text("LOGIN"),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Belum punya akun ?"),
              TextButton(
                onPressed: () => Get.toNamed(Routes.SIGNUP),
                child: Text('Daftar Sekarang'),
              ),
            ],
          )
        ],
      ),
    );
  }
}
