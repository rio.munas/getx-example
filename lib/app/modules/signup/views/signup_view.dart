import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/signup_controller.dart';

class SignupView extends GetView<SignupController> {
  final emailC = TextEditingController();
  final passC = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('SignupView'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(25.0),
        child: Column(children: [
          TextField(
            controller: emailC,
            decoration: InputDecoration(labelText: 'E-mail'),
          ),
          TextField(
            controller: passC,
            decoration: InputDecoration(labelText: 'Password'),
            obscureText: true,
          ),
          SizedBox(
            height: 35,
          ),
          ElevatedButton(
            onPressed: () {
              controller.singup(emailC.text, passC.text);
            },
            child: Text("SIGNUP"),
          ),
        ]),
      ),
    );
  }
}
